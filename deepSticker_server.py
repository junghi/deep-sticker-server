import copy
import json
import os
import random
import time
from datetime import datetime

import cv2

import imutils
from flask import Flask, render_template, jsonify, make_response, send_file
from flask import request
from flask import Response
from flask import stream_with_context
from PIL import Image
import numpy as np
import requests


# # load trained model


app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True


@app.route('/')
def home():
    return render_template('index.html')


# #########################################################


version = '0.1.0'
if __name__ == '__main__':
    print('------------------------------------------------')
    print('DeepSticker - Cutting Recognition')
    print('------------------------------------------------')

    # app.debug = True
    app.run(host='0.0.0.0', port=5000)
